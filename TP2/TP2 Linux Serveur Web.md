# TP2 Linux Serveur Web

## Prérequis


```bash

### modifier fichier /etc/hostname
#
#Sur la 1er machine 

#modifier le nom de la machine 

$ echo 'web.tp2.cesi' | sudo tee /etc/hostname
 
$ cat /etc/hostname
web.tp2.cesi

# modifier l'@ ip de la machine 

$ cd /etc/sysconfig/network-scripts/
$ sudo vim ifcfg-enp0s8


TYPE=Ethernet
BOOTPROTO=static
NAME=enp0s8
DEVICE=enp0s8
ONBOOT=yes
IPADDR=10.55.55.20
NETMASK=255.255.255.0

#vérification 

 ip a

3: **enp0s8:** <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc pfifo_fast state UP group default qlen 1000
    link/ether 08:00:27:25:4d:d2 brd ff:ff:ff:ff:ff:ff
    inet **10.55.55.20/24** brd 10.55.55.255 scope global noprefixroute enp0s8
       valid_lft forever preferred_lft forever
    inet6 fe80::a00:27ff:fe25:4dd2/64 scope link
       valid_lft forever preferred_lft forever
[thaoanh@web ~]$                                                                                                       

#Sur la 2eme machine 

#modifier nom de machine 

$ echo 'db.tp2.cesi' | sudo tee /etc/hostname

$ cat /etc/hostname
db.tp2.cesi

#modifer l"@ IP de la machine 

$ cd /etc/sysconfig/network-scripts/
$ sudo vim ifcfg-enp0s8

TYPE=Ethernet
BOOTPROTO=static
NAME=enp0s8
DEVICE=enp0s8
ONBOOT=yes
IPADDR=10.55.55.21
NETMASK=255.255.255.0

# vérification 

 ip a

3: enp0s8: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc pfifo_fast state UP group default qlen 1000
    link/ether 08:00:27:72:d6:96 brd ff:ff:ff:ff:ff:ff
    inet **10.55.55.21/24** brd 10.55.55.255 scope global noprefixroute enp0s8
       valid_lft forever preferred_lft forever
    inet6 fe80::a00:27ff:fe72:d696/64 scope link
       valid_lft forever preferred_lft forever
[thaoanh@db ~]$       

#Sur la 3eme machine 

#modifier le nom de la machine 

$ echo 'rp.tp2.cesi' | sudo tee /etc/hostname

$ cat /etc/hostname
rp.tp2.cesi

#modifier l' @ IP de la machine

$cd /etc/sysconfig/network-scripts/
$sudo vim ifcfg-enp0s8

TYPE=Ethernet
BOOTPROTO=static
NAME=enp0s8
DEVICE=enp0s8
ONBOOT=yes
IPADDR=10.55.55.22
NETMASK=255.255.255.0

# vérification 


 ip a

3: enp0s8: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc pfifo_fast state UP group default qlen 1000
    link/ether 08:00:27:76:d9:e9 brd ff:ff:ff:ff:ff:ff
    inet **10.55.55.22/24** brd 10.55.55.255 scope global noprefixroute enp0s8
       valid_lft forever preferred_lft forever
    inet6 fe80::a00:27ff:fe76:d9e9/64 scope link
       valid_lft forever preferred_lft forever
[thaoanh@rp ~]$


```

### modification  fichier /etc/hosts


```bash

#il faut attribuer des noms d'hôtes à chacune des adresses IP.

# sur la 1er machine 

#modifier le fichier /etc/hosts

127.0.0.1   localhost localhost.localdomain localhost4 localhost4.localdomain4
::1         localhost localhost.localdomain localhost6 localhost6.localdomain6

#rajouter ces 3 lignes

10.55.55.20     web.tp2.cesi    web
10.55.55.21     db.tp2.cesi     db
10.55.55.22     rp.tp2.cesi     rp

# sur la 2eme machine 

#modifier le fichier /etc/hosts

127.0.0.1   localhost localhost.localdomain localhost4 localhost4.localdomain4
::1         localhost localhost.localdomain localhost6 localhost6.localdomain6

#rajouter ces 3 lignes

10.55.55.20     web.tp2.cesi    web
10.55.55.21     db.tp2.cesi     db
10.55.55.22     rp.tp2.cesi     rp

# sur la 3emem machine 

#modifier le fichier /etc/hosts

127.0.0.1   localhost localhost.localdomain localhost4 localhost4.localdomain4
::1         localhost localhost.localdomain localhost6 localhost6.localdomain6

#rajouter ces 3 lignes

10.55.55.20     web.tp2.cesi    web
10.55.55.21     db.tp2.cesi     db
10.55.55.22     rp.tp2.cesi     rp

$ sudo setenforce 0
$ sudo sed -i 's/SELINUX=enforcing/SELINUX=permissive/g' /etc/selinux/config
```

## BDD

```bash=

# Installer le paquet mariadb-server
$ sudo yum install mariadb-server

#démarrer le service 
$ sudo systemctl start mariadb

#vérifier le status 
$ sudo systemctl status mariadb

● mariadb.service - MariaDB database server
   Loaded: loaded (/usr/lib/systemd/system/mariadb.service; disabled; vendor preset: disabled)
  ** Active: active (running)** since Wed 2020-12-16 13:05:23 CET; 8s ago
  Process: 1620 ExecStartPost=/usr/libexec/mariadb-wait-ready $MAINPID (code=exited, status=0/SUCCESS)
  Process: 1537 ExecStartPre=/usr/libexec/mariadb-prepare-db-dir %n (code=exited, status=0/SUCCESS)
 Main PID: 1619 (mysqld_safe)
   CGroup: /system.slice/mariadb.service
           ├─1619 /bin/sh /usr/bin/mysqld_safe --basedir=/usr
           └─1784 /usr/libexec/mysqld --basedir=/usr --datadir=/var/lib/mysql --plugin-dir=/usr/lib64/mysql/plugin --...

Dec 16 13:05:21 db.tp2.cesi mariadb-prepare-db-dir[1537]: MySQL manual for more instructions.
Dec 16 13:05:21 db.tp2.cesi mariadb-prepare-db-dir[1537]: Please report any problems at http://mariadb.org/jira
Dec 16 13:05:21 db.tp2.cesi mariadb-prepare-db-dir[1537]: The latest information about MariaDB is available at ht...rg/.
Dec 16 13:05:21 db.tp2.cesi mariadb-prepare-db-dir[1537]: You can find additional information about the MySQL part at:
Dec 16 13:05:21 db.tp2.cesi mariadb-prepare-db-dir[1537]: http://dev.mysql.com
Dec 16 13:05:21 db.tp2.cesi mariadb-prepare-db-dir[1537]: Consider joining MariaDB's strong and vibrant community:
Dec 16 13:05:21 db.tp2.cesi mariadb-prepare-db-dir[1537]: https://mariadb.org/get-involved/
Dec 16 13:05:22 db.tp2.cesi mysqld_safe[1619]: 201216 13:05:22 mysqld_safe Logging to '/var/log/mariadb/mariadb.log'.
Dec 16 13:05:22 db.tp2.cesi mysqld_safe[1619]: 201216 13:05:22 mysqld_safe Starting mysqld daemon with databases...mysql
Dec 16 13:05:23 db.tp2.cesi systemd[1]: Started MariaDB database server.
Hint: Some lines were ellipsized, use -l to show in full.

```

```bash

#création lien symbolique 
$ sudo systemctl enable mariadb

#se connecter à la base 
$ mysql -u root -p

$  CREATE DATABASE ma_bdd;  

show databases;
+--------------------+
| Database           |
+--------------------+
| information_schema |
| ma_bdd             |
| mysql              |
| performance_schema |
| test               |
+--------------------+

$ CREATE USER 'toto'@'10.55.55.20' IDENTIFIED BY 'root';

$ select user, host  from mysql.user;

+------+-------------+
| user | host        |
+------+-------------+
| toto | 10.55.55.20 |
| root | 127.0.0.1   |
| root | ::1         |
| root | db.tp2.cesi |
| root | localhost   |
+------+-------------+

#attribuer les droits sur la base de données à l'utilisateur
$ GRANT ALL PRIVILEGES ON ma_bdd.* TO toto@10.55.55.20;


# vérifier les droits
$ show grants for "toto"@"10.55.55.20";

+---------------------------------------------------------------------------------------------------------------+
| Grants for toto@10.55.55.20                                                                                   |
+---------------------------------------------------------------------------------------------------------------+
| GRANT USAGE ON *.* TO 'toto'@'10.55.55.20' IDENTIFIED BY PASSWORD '*81F5E21E35407D884A6CD4A731AEBFB6AF209E1B' |
| GRANT ALL PRIVILEGES ON `ma_bdd`.* TO 'toto'@'10.55.55.20'                                                    |
+---------------------------------------------------------------------------------------------------------------+



#connaitre le port utilisé par la base de données
$ sudo ss -alnpt

State      Recv-Q Send-Q               Local Address:Port                              Peer Address:Port
LISTEN     0      5                                *:8888                                         *:*                   users:(("python2",pid=739,fd=3))
LISTEN     0      100                      127.0.0.1:25                                           *:*                   users:(("master",pid=1430,fd=13))
LISTEN     0      50                               *:3306                                         *:*                   users:(("mysqld",pid=1381,fd=14))
LISTEN     0      128                              *:22                                           *:*                   users:(("sshd",pid=1135,fd=3))
LISTEN     0      100                          [::1]:25                                        [::]:*                   users:(("master",pid=1430,fd=14))
LISTEN     0      128                           [::]:80                                        [::]:*                   users:(("httpd",pid=1648,fd=4),("httpd",pid=1647,fd=4),("httpd",pid=1646,fd=4),("httpd",pid=1645,fd=4),("httpd",pid=1644,fd=4),("httpd",pid=1144,fd=4))
LISTEN     0      128                           [::]:22                                        [::]:*                   users:(("sshd",pid=1135,fd=4))




#Apache utilise le port 80 en TCP pour le protocole HTTP. Il faudra donc songer à ouvrir ce port dans le pare-feu.
$ sudo firewall-cmd --add-port=80/tcp --permanent
success

$ sudo firewall-cmd --reload

#MySQL écoute par défaut sur le port TCP 3306. Il faudra donc songer à ouvrir ce port dans le pare-feu.
$ sudo firewall-cmd --add-port=3306/tcp --permanent

$ sudo firewall-cmd --reload

$ sudo firewall-cmd --add-port=443/tcp --permanent

$ sudo firewall-cmd --reload

#Confirmez que les ports on été ouvert avec succès:

$ sudo firewall-cmd --list-all
public (active)
  target: default
  icmp-block-inversion: no
  interfaces: enp0s3 enp0s8
  sources:
  services: dhcpv6-client http ssh
  ports: 80/tcp 8888/tcp 443/tcp 3306/tcp
  protocols:
  masquerade: no
  forward-ports:
  source-ports:
  icmp-blocks:
  rich rules:

```
 ## web
 
 ### installer le serveur web Apache 
```bash

#Installer un serveur Apache / le serveur Apache est fourni par le paquet httpd.
$ sudo yum install httpd

#démarrer le service 
$ sudo systemctl start httpd

#vérifier le service 
$ sudo systemctl status httpd

$ sudo systemctl status httpd

httpd.service - The Apache HTTP Server
   Loaded: loaded (/usr/lib/systemd/system/httpd.service; disabled; vendor preset: disabled)
   Active: active (running) since Wed 2020-12-16 23:37:10 CET; 20s ago
     Docs: man:httpd(8)
           man:apachectl(8)
 Main PID: 1718 (httpd)
   Status: "Total requests: 0; Current requests/sec: 0; Current traffic:   0 B/sec"
   CGroup: /system.slice/httpd.service
           ├─1718 /usr/sbin/httpd -DFOREGROUND
           ├─1719 /usr/sbin/httpd -DFOREGROUND
           ├─1720 /usr/sbin/httpd -DFOREGROUND
           ├─1721 /usr/sbin/httpd -DFOREGROUND
           ├─1722 /usr/sbin/httpd -DFOREGROUND
           └─1723 /usr/sbin/httpd -DFOREGROUND

Dec 16 23:37:10 web.tp2.cesi systemd[1]: Starting The Apache HTTP Server...
Dec 16 23:37:10 web.tp2.cesi systemd[1]: Started The Apache HTTP Server.

$ sudo systemctl enable httpd --now
Created symlink from /etc/systemd/system/multi-user.target.wants/httpd.service to /usr/lib/systemd/system/httpd.service.


# Installation de dépôt additionels
$ sudo yum install http://rpms.remirepo.net/enterprise/remi-release-7.rpm -y
$ sudo yum install -y yum-utils

# On supprime d'éventuelles vieilles versions de PHP précédemment installées
$ sudo yum remove -y php

# Activation du nouveau dépôt
$ sudo yum-config-manager --enable remi-php56  

# Installation de PHP 5.6.40 et de librairies récurrentes
$ sudo yum install php php-mcrypt php-cli php-gd php-curl php-mysql php-ldap php-zip php-fileinfo -y

```

#### télécharger wordpress


```bash=

#via WinSCP j'ai déposé le dossier zip dans /var/www/html

$ cd /var/www/html
$ ls

 wordpress-5.6.tar.gz
 
#extraire l'archive wordpress dans un dossier qui pourra être servi par Apache

#pour décompresser un fichier tar 
$ tar xzf wordpress-5.6.tar.gz

$ ls

$wordpress  $wordpress-5.6.tar.gz


#Installer le navigateur en mode texte ELinks.
$ sudo yum install elinks

Loaded plugins: fastestmirror
Loading mirror speeds from cached hostfile
 * base: mirroir.wptheme.fr
 * extras: centos.quelquesmots.fr
 * updates: centos.quelquesmots.fr
Resolving Dependencies
--> Running transaction check
---> Package elinks.x86_64 0:0.12-0.37.pre6.el7.0.1 will be installed
--> Processing Dependency: libnss_compat_ossl.so.0()(64bit) for package: elinks-0.12-0.37.pre6.el7.0.1.x86_64
--> Processing Dependency: libmozjs185.so.1.0()(64bit) for package: elinks-0.12-0.37.pre6.el7.0.1.x86_64
--> Running transaction check
---> Package js.x86_64 1:1.8.5-20.el7 will be installed
---> Package nss_compat_ossl.x86_64 0:0.9.6-8.el7 will be installed
--> Finished Dependency Resolution

Dependencies Resolved

=======================================================================================================================
 Package                       Arch                 Version                                Repository             Size
=======================================================================================================================
Installing:
 elinks                        x86_64               0.12-0.37.pre6.el7.0.1                 updates               882 k
Installing for dependencies:
 js                            x86_64               1:1.8.5-20.el7                         base                  2.3 M
 nss_compat_ossl               x86_64               0.9.6-8.el7                            base                   37 k

Transaction Summary
=======================================================================================================================
Install  1 Package (+2 Dependent packages)

Total download size: 3.2 M
Installed size: 9.6 M
Is this ok [y/d/N]: y

#Tester le bon fonctionnement du serveur.
elinks http://localhost

#résultat 

 Apache HTTP Server Test Page powered by CentOS  
 
      Testing 123..                                                                                                                                                                               This page is used to test the proper operation of the Apache HTTP server after it has been installed. If you can                 read this page it means that this site is working properly. This server is powered by CentOS.                                                                                                                                    Just visiting?                                                                                                                                                                                                                                   The website you just visited is either experiencing problems or is undergoing routine maintenance.                                                                                                                                            If you would like to let the administrators of this website know that you've seen this page instead of the page        you expected, you should send them e-mail. In general, mail sent to the name "webmaster" and directed to the           website's domain should reach the appropriate person.                                                                                                                                                                                         For example, if you experienced problems while visiting www.example.com, you should send e-mail to                     "webmaster@example.com".   
      
      
 ##modifier fichier de conf
 
 
$ cd wordpress
$ ls
 
 
 
 index.php    wp-activate.php     wp-comments-post.php  wp-cron.php        wp-load.php   wp-settings.php   xmlrpc.php
license.txt  wp-admin            wp-config-sample.php  wp-includes        wp-login.php  wp-signup.php
readme.html  wp-blog-header.php  wp-content            wp-links-opml.php  wp-mail.php   wp-trackback.php
 
 
$ sudo cp wp-config-sample.php wp-config.php
 
$ ls

index.php        wp-admin              wp-config-sample.php  wp-links-opml.php  wp-settings.php
license.txt      wp-blog-header.php    wp-content            wp-load.php        wp-signup.php
readme.html      wp-comments-post.php  wp-cron.php           wp-login.php       wp-trackback.php
wp-activate.php  wp-config.php         wp-includes           wp-mail.php        xmlrpc.php


define( 'DB_NAME', 'ma_bdd' );

/** MySQL database username */
define( 'DB_USER', 'toto' );

/** MySQL database password */
define( 'DB_PASSWORD', 'root' );

/** MySQL hostname */
define( 'DB_HOST', '10.55.55.21' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );
```

## Reverse Proxy

```bash=

#Installer un serveur NGINX (paquet epel-release puis nginx sur CentOS).

$ sudo yum install epel-release

Installed:
  epel-release.noarch 0:7-11

Complete!

$ sudo yum update
$ sudo yum install nginx

$ sudo firewall-cmd --add-port=80/tcp --permanent

$ sudo systemctl start nginx
$ sudo systemctl enable nginx

$ sudo systemctl status nginx
● nginx.service - The nginx HTTP and reverse proxy server
   Loaded: loaded (/usr/lib/systemd/system/nginx.service; enabled; vendor preset: disabled)
   Active: active (running) since Thu 2020-12-17 01:06:57 CET; 1min 24s ago
 Main PID: 9593 (nginx)
   CGroup: /system.slice/nginx.service
           ├─9593 nginx: master process /usr/sbin/nginx
           └─9594 nginx: worker process

Dec 17 01:06:56 rp.tp2.cesi systemd[1]: Starting The nginx HTTP and reverse proxy server...
Dec 17 01:06:57 rp.tp2.cesi nginx[9588]: nginx: the configuration file /etc/nginx/nginx.conf syntax is ok
Dec 17 01:06:57 rp.tp2.cesi nginx[9588]: nginx: configuration file /etc/nginx/nginx.conf test is successful
Dec 17 01:06:57 rp.tp2.cesi systemd[1]: Failed to parse PID from file /run/nginx.pid: Invalid argument
Dec 17 01:06:57 rp.tp2.cesi systemd[1]: Started The nginx HTTP and reverse proxy server.
[thaoanh@rp ~]$


#On autorise dans le pare-feu le protocole http (ici si la zone de l'interface est public) :

$ sudo firewall-cmd --permanent --zone=public --add-service=http
success

$ sudo firewall-cmd --permanent --zone=public --add-service=https
success

$ sudo firewall-cmd --reload
success

#dans le doute je fais aussi 
$ sudo firewall-cmd --add-port=80/tcp --permanent
success


```
### Configurer NGINX 

```bash=

$ cd /etc/nginx/

$ ls

conf.d        fastcgi.conf.default    koi-utf     mime.types.default  scgi_params          uwsgi_params.default
default.d     fastcgi_params          koi-win     nginx.conf          scgi_params.default  win-utf
fastcgi.conf  fastcgi_params.default  mime.types  nginx.conf.default  uwsgi_params

$ sudo vim nginx.conf

events {}

http {
    server {
        listen       80;

        server_name web.tp2.cesi;

        location / {
            proxy_pass   http://10.55.55.20:80;
        }
    }
}

~        
```
### s'assurer qu'on a bien ouvert les ports

```bash=
$ sudo firewall-cmd --add-port=8888/tcp --permanent
$ sudo firewall-cmd --add-port=80/tcp --permanent
$ sudo firewall-cmd --add-port=443/tcp --permanent
$ sudo firewall-cmd --add-port=3306/tcp --permanent

$ sudo firewall-cmd --list-all
public (active)
  target: default
  icmp-block-inversion: no
  interfaces: enp0s3 enp0s8
  sources:
  services: dhcpv6-client http https ssh
  ports: 8888/tcp 80/tcp 443/tcp 3306/tcp
  protocols:
  masquerade: no
  forward-ports:
  source-ports:
  icmp-blocks:
  rich rules:
  
  #Tester qu'on accède bien au Wordpress en passant par l'IP du reverse proxy.
  
  http://10.55.55.22/wordpress/ 
  
  # ca me redirige vers l'address du serveur web et affichie bien wordpress
  
  
http://10.55.55.20/wordpress/wp-admin/install.php

#j'ai modifié le fichier hosts de ma machine

10.55.55.20     web.tp2.cesi    
10.55.55.21     db.tp2.cesi     
10.55.55.22     rp.tp2.cesi  

#si je test avec ca :

http://web.tp2.cesi/wordpress/wp-admin/install.php

#ca fonctionne aussi et également avec ca

http://rp.tp2.cesi/wordpress/wp-admin/install.php


#vérif

$ curl http://10.55.55.22/wordpress


<!DOCTYPE HTML PUBLIC "-//IETF//DTD HTML 2.0//EN">
<html><head>
<title>301 Moved Permanently</title>
</head><body>
<h1>Moved Permanently</h1>
<p>The document has moved <a href="http://10.55.55.20/wordpress/">here</a>.</p>
</body></html>
```
## Sécu

```bash=

#fait sur les 3 machines 

$ sudo yum install fail2ban-server
$ sudo yum install fail2ban-firewalld
$ sudo systemctl start fail2ban
$ sudo systemctl status fail2ban
$ sudo systemctl enable fail2ban

$ sudo vim /etc/fail2ban/jail.local

#on modifie le fichier jail.local

[[DEFAULT]
# Ban hosts for one hour:
bantime = 3600

# Override /etc/fail2ban/jail.d/00-firewalld.conf:
banaction = iptables-multiport

[sshd]
enabled     = true
#port        = ssh
#filter      = sshd
logpath     = /var/log/auth.log
maxretry    = 3
bantime     = 30


# redémarrer le fail2ban service 
$ sudo systemctl restart fail2ban

#vérifier le status 
$ sudo systemctl status fail2ban



● fail2ban.service - Fail2Ban Service
   Loaded: loaded (/usr/lib/systemd/system/fail2ban.service; enabled; vendor preset: disabled)
   Active: active (running) since Thu 2020-12-17 07:18:42 CET; 6s ago
     Docs: man:fail2ban(1)
  Process: 5634 ExecStop=/usr/bin/fail2ban-client stop (code=exited, status=0/SUCCESS)
  Process: 5639 ExecStartPre=/bin/mkdir -p /run/fail2ban (code=exited, status=0/SUCCESS)
 Main PID: 5640 (f2b/server)
   CGroup: /system.slice/fail2ban.service
           └─5640 /usr/bin/python2 -s /usr/bin/fail2ban-server -xf start

Dec 17 07:18:42 web.tp2.cesi systemd[1]: Stopped Fail2Ban Service.
Dec 17 07:18:42 web.tp2.cesi systemd[1]: Starting Fail2Ban Service...
Dec 17 07:18:42 web.tp2.cesi systemd[1]: Started Fail2Ban Service.
Dec 17 07:18:42 web.tp2.cesi fail2ban-server[5640]: Server ready

# Afin de vérifier que le service fonctionne, nous pouvons utiliser fail2ban-client:
$ sudo fail2ban-client status
Status
|- Number of jail:      1
`- Jail list:   sshd

```bash=

#depuis mon serveur web, j essaye de me connecter a mon serveur db

PS C:\Users\Karine> ssh thaoanh@10.55.55.20
[thaoanh@web ~]$


[thaoanh@web ~]$ ssh thaoanh@10.55.55.21
thaoanh@10.55.55.21's password:
Permission denied, please try again.
thaoanh@10.55.55.21's password:
Permission denied, please try again.
thaoanh@10.55.55.21's password:
Permission denied (publickey,gssapi-keyex,gssapi-with-mic,password).
[thaoanh@web ~]$ ssh thaoanh@10.55.55.21
ssh: connect to host 10.55.55.21 port 22: Connection refused
[thaoanh@web ~]$

```
```bash=

#depuis mon serveur db j'essaye de me connecter a mon serveur rp

PS C:\Users\Karine> ssh thaoanh@10.55.55.21
[thaoanh@db ~]$

[thaoanh@db ~]$ ssh thaoanh@10.55.55.22
The authenticity of host '10.55.55.22 (10.55.55.22)' can't be established.
ECDSA key fingerprint is SHA256:iM2Y7idMe4MkVp7fMy7NKCksX4fcjwwVyI7Fk4Nnajg.
ECDSA key fingerprint is MD5:ad:f9:77:c5:bf:31:61:df:b7:cd:64:ea:34:2a:ef:74.
Are you sure you want to continue connecting (yes/no)? yes
Warning: Permanently added '10.55.55.22' (ECDSA) to the list of known hosts.
thaoanh@10.55.55.22's password:
Permission denied, please try again.
thaoanh@10.55.55.22's password:
Permission denied, please try again.
thaoanh@10.55.55.22's password:
Permission denied (publickey,gssapi-keyex,gssapi-with-mic,password).
[thaoanh@db ~]$ ssh thaoanh@10.55.55.22
ssh: connect to host 10.55.55.22 port 22: Connection refused
[thaoanh@db ~]$

```
```bash=

#depuis mon serveur rp j'essaye de me connecter a mon serveur web

PS C:\Users\Karine> ssh thaoanh@10.55.55.22

 ssh thaoanh@10.55.55.20
 [thaoanh@rp ~]$


[thaoanh@rp ~]$ ssh thaoanh@10.55.55.20
thaoanh@10.55.55.20's password:
Permission denied, please try again.
thaoanh@10.55.55.20's password:
Permission denied, please try again.
thaoanh@10.55.55.20's password:
Permission denied (publickey,gssapi-keyex,gssapi-with-mic,password).
[thaoanh@rp ~]$ ssh thaoanh@10.55.55.20
ssh: connect to host 10.55.55.20 port 22: Connection refused
[thaoanh@rp ~]$


